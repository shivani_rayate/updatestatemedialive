const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');

const awsConfig = require('../../../config/aws_config')['development'];


//AWS Credentials 
AWS.config.update({
  accessKeyId: awsConfig.accessKeyId,
  secretAccessKey: awsConfig.secretAccessKey,
  region: awsConfig.region,
  rolarn: awsConfig.RoleArn
});

//Declaraation 
const medialive = new AWS.MediaLive();
const mediapackage = new AWS.MediaPackage();
const ssm = new AWS.SSM();

// aws media live channel list 
exports.listChannels = reqParams => new Promise((resolve, reject) => {

  medialive.listChannels(reqParams, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //console.log('List Of channels :-  ' + JSON.stringify(data));
      resolve(data)
    }
  });

})


// aws media package channel list
exports.listChannelsmediapackage = reqParams => new Promise((resolve, reject) => {

  mediapackage.listChannels(reqParams, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //  console.log('List Of channels on mediapackage is ' + JSON.stringify(data));
      resolve(data)
    }
  });

})

//aws media live input list 
exports.listInputs = reqParams => new Promise((resolve, reject) => {

  var params = {
    // MaxResults: '',
    // NextToken: 'STRING_VALUE'
  };

  medialive.listInputs(params, function (err, data) {
    if (err) {
      reject(err)
    } else {
      // console.log('listInputs is ' + JSON.stringify(data));
      resolve(data)
    }
  });

})

// aws media live create input 
exports.createInput = reqParams => new Promise((resolve, reject) => {

  var params = {

    Destinations: [
      {
        StreamName: reqParams.application_A + '/' + reqParams.instance_A,

      },
      {
        StreamName: reqParams.application_B + '/' + reqParams.instance_B
      },

    ],

    InputSecurityGroups: [
      '2836666'
      // reqParams.InputSecurityGroups,
      /* more items */
    ],

    Name: reqParams.Name,
    // RoleArn: 'arn:aws:medialive:ap-south-1:705991774343:inputSecurityGroup:3263688',

    Type: reqParams.Type,

  };

  //console.log("createInput in utility" + (params))

  medialive.createInput(params, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //  console.log('listInputSecurityGroups is ' + JSON.stringify(data));
      resolve(data)
    }
  });

})

//aws media live input security group listing 
exports.listInputSecurityGroups = reqParams => new Promise((resolve, reject) => {

  var params = {
    MaxResults: '4',
    //  NextToken :'5pDChLZard/YnNGwpSRZtf46mdSFpcB0t3pI42DqN4Nrk/aiOtqtYOlXiNOyDAXH2JRZIEJVW0Gcsg/j00MOic14d5SyDSKv48nAa/pvaavIrBR0C4rnlaCAKBjeVkrg'

  };

  medialive.listInputSecurityGroups(params, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //  console.log('listInputSecurityGroups is ' + JSON.stringify(data));
      resolve(data)
    }
  });


})

//aws media package channel creation
exports.createChannel = reqParams => new Promise((resolve, reject) => {

  var params = {
    Id: reqParams.Id,
    // description: reqParams.description,
  };

  mediapackage.createChannel(params, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //console.log('mediapackage createChannel is ' + JSON.stringify(data));
      resolve(data)
    }
  });

})

//aws media pacakge endpoint creation 
exports.createOriginEndpoint = reqParams => new Promise((resolve, reject) => {

  var params = {
    // ChannelId: reqParams.ChannelId, /* required */
    // Id: reqParams.ChannelId, /* required */
    ChannelId: reqParams.id,
    // Id: reqParams.id,
    Id: reqParams.packageID,
    HlsPackage: {
      AdMarkers: 'NONE',
      AdTriggers: [
        'SPLICE_INSERT'

      ],
      AdsOnDeliveryRestrictions: 'RESTRICTED',
      IncludeIframeOnlyStream: false,
      PlaylistType: 'EVENT',
      PlaylistWindowSeconds: '60',
      ProgramDateTimeIntervalSeconds: '0',
      SegmentDurationSeconds: '6',
      StreamSelection: {

        StreamOrder: 'ORIGINAL'
      },
      UseAudioRenditionGroup: false,
    },


    Origination: 'ALLOW',
    StartoverWindowSeconds: '0',
    TimeDelaySeconds: '0',

  };

  //console.log('createOriginEndpoint params ' + JSON.stringify(params));
  mediapackage.createOriginEndpoint(params, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //console.log('createOriginEndpoint is ' + JSON.stringify(data));
      resolve(data)
    }
  });


})

//aws media live create channel 
exports.createChannelmedialive = reqParams => new Promise((resolve, reject) => {


  if (reqParams.profileType === 'SD') {

    //console.log("In SD Section : >>>>>>>>>>>")
    // SD JSON
    var params = {

      InputAttachments: [
        {
          InputId: reqParams.Input_ID,
          InputAttachmentName: reqParams.Input_Name,
          InputSettings: {
            SourceEndBehavior: "CONTINUE",
            InputFilter: "AUTO",
            FilterStrength: 1,
            DeblockFilter: "DISABLED",
            DenoiseFilter: "DISABLED",
            AudioSelectors: [],
            CaptionSelectors: []
          }
        }
      ],

      Destinations: [
        {
          Id: "ti54mj",
          Settings: [
            {
              PasswordParam: reqParams.password_A,

              Url: reqParams.URL_A,
              Username: reqParams.username_A

            },
            {

              Url: reqParams.URL_B,
              Username: reqParams.username_B,

              PasswordParam: reqParams.password_B
            },
            /* more items */
          ],
          MediaPackageSettings: []
        }
      ],

      EncoderSettings: {
        AudioDescriptions: [
          {

            AudioSelectorName: "audio_u2dyi",
            CodecSettings: {
              AacSettings: {
                InputType: "NORMAL",
                Bitrate: 64000,
                CodingMode: "CODING_MODE_2_0",
                RawFormat: "NONE",
                Spec: "MPEG4",
                Profile: "LC",
                RateControlMode: "CBR",
                SampleRate: 48000
              }
            },

            AudioTypeControl: "FOLLOW_INPUT",
            LanguageCodeControl: "FOLLOW_INPUT",
            Name: "audio_yxa4e6"
          }
        ],
        CaptionDescriptions: [],
        OutputGroups: [
          {
            OutputGroupSettings: {
              HlsGroupSettings: {
                AdMarkers: [],
                CaptionLanguageSetting: "OMIT",
                CaptionLanguageMappings: [],
                HlsCdnSettings: {
                  HlsWebdavSettings: {
                    NumRetries: 10,
                    ConnectionRetryInterval: 1,
                    RestartDelay: 15,
                    FilecacheDuration: 300,
                    HttpTransferMode: "NON_CHUNKED"
                  }
                },
                InputLossAction: "EMIT_OUTPUT",
                ManifestCompression: "NONE",
                Destination: {
                  DestinationRefId: "ti54mj"
                },
                IvInManifest: "INCLUDE",
                IvSource: "FOLLOWS_SEGMENT_NUMBER",
                ClientCache: "ENABLED",
                TsFileMode: "SEGMENTED_FILES",
                ManifestDurationFormat: "FLOATING_POINT",
                SegmentationMode: "USE_SEGMENT_DURATION",
                RedundantManifest: "DISABLED",
                OutputSelection: "MANIFESTS_AND_SEGMENTS",
                StreamInfResolution: "INCLUDE",
                IFrameOnlyPlaylists: "DISABLED",
                IndexNSegments: 10,
                ProgramDateTime: "EXCLUDE",
                ProgramDateTimePeriod: 600,
                KeepSegments: 21,
                SegmentLength: 10,
                TimedMetadataId3Frame: "PRIV",
                TimedMetadataId3Period: 10,
                HlsId3SegmentTagging: "DISABLED",
                CodecSpecification: "RFC_4281",
                DirectoryStructure: "SINGLE_DIRECTORY",
                SegmentsPerSubdirectory: 10000,
                Mode: "LIVE"
              }
            },
            Name: "mk_hls",
            Outputs: [
              {
                OutputSettings: {
                  HlsOutputSettings: {
                    NameModifier: "_1",
                    HlsSettings: {
                      StandardHlsSettings: {
                        M3u8Settings: {
                          AudioFramesPerPes: 4,
                          AudioPids: "492-498",
                          NielsenId3Behavior: "NO_PASSTHROUGH",
                          PcrControl: "PCR_EVERY_PES_PACKET",
                          PmtPid: "480",
                          ProgramNum: 1,
                          Scte35Pid: "500",
                          Scte35Behavior: "NO_PASSTHROUGH",
                          TimedMetadataPid: "502",
                          TimedMetadataBehavior: "NO_PASSTHROUGH",
                          VideoPid: "481"
                        },
                        AudioRenditionSets: "program_audio"
                      }
                    },
                    H265PackagingType: "HVC1"
                  }
                },
                OutputName: "ncyujg",
                VideoDescriptionName: "video_rq0wix",
                AudioDescriptionNames: [
                  "audio_yxa4e6"
                ],
                CaptionDescriptionNames: []
              }
            ]
          }
        ],
        TimecodeConfig: {
          Source: "EMBEDDED"
        },
        VideoDescriptions: [
          {
            CodecSettings: {
              H264Settings: {
                AfdSignaling: "NONE",
                ColorMetadata: "INSERT",
                AdaptiveQuantization: "MEDIUM",
                Bitrate: 3200000,
                EntropyEncoding: "CABAC",
                FlickerAq: "ENABLED",
                FramerateControl: "INITIALIZE_FROM_SOURCE",
                GopBReference: "DISABLED",
                GopClosedCadence: 1,
                GopNumBFrames: 2,
                GopSize: 90,
                GopSizeUnits: "FRAMES",
                SubgopLength: "FIXED",
                ScanType: "PROGRESSIVE",
                Level: "H264_LEVEL_AUTO",
                LookAheadRateControl: "MEDIUM",
                MaxBitrate: 3200000,
                NumRefFrames: 1,
                ParControl: "INITIALIZE_FROM_SOURCE",
                Profile: "MAIN",
                RateControlMode: "QVBR",
                QvbrQualityLevel: 8,
                Syntax: "DEFAULT",
                SceneChangeDetect: "ENABLED",
                SpatialAq: "ENABLED",
                TemporalAq: "ENABLED",
                TimecodeInsertion: "DISABLED"
              }
            },
            Height: 720,
            Name: "video_rq0wix",
            RespondToAfd: "NONE",
            Sharpness: 50,
            ScalingBehavior: "DEFAULT",
            Width: 1080
          }
        ]
      },

      InputSpecification: {
        Codec: "AVC",
        Resolution: "HD",
        MaximumBitrate: "MAX_20_MBPS"
      },
      LogLevel: 'INFO',
      Name: reqParams.channelName,
      RequestId: reqParams.requestId,
      RoleArn: 'arn:aws:iam::705991774343:role/MediaLiveAccessRole',
      Tags: {
      },
      ChannelClass: "STANDARD",

    }

  } else {

    console.log("In HD Section : >>>>>>>>>>>")

    // HD JSON

    var params = {

      InputAttachments: [
        {
          InputId: reqParams.Input_ID,
          InputAttachmentName: reqParams.Input_Name,
          InputSettings: {
            SourceEndBehavior: "CONTINUE",
            InputFilter: "AUTO",
            FilterStrength: 1,
            DeblockFilter: "DISABLED",
            DenoiseFilter: "DISABLED",
            AudioSelectors: [],
            CaptionSelectors: []
          }
        }
      ],

      Destinations: [
        {
          Id: "ti54mj",
          Settings: [
            {
              PasswordParam: reqParams.password_A,

              Url: reqParams.URL_A,
              Username: reqParams.username_A

            },
            {

              Url: reqParams.URL_B,
              Username: reqParams.username_B,

              PasswordParam: reqParams.password_B
            },
            /* more items */
          ],
          MediaPackageSettings: []
        }
      ],

      EncoderSettings: {
        AudioDescriptions: [
          {

            AudioSelectorName: "audio_u2dyi",
            CodecSettings: {
              AacSettings: {
                InputType: "NORMAL",
                Bitrate: 64000,
                CodingMode: "CODING_MODE_2_0",
                RawFormat: "NONE",
                Spec: "MPEG4",
                Profile: "LC",
                RateControlMode: "CBR",
                SampleRate: 48000
              }
            },

            AudioTypeControl: "FOLLOW_INPUT",
            LanguageCodeControl: "FOLLOW_INPUT",
            Name: "audio_yxa4e6"
          }
        ],
        CaptionDescriptions: [],
        OutputGroups: [
          {
            OutputGroupSettings: {
              HlsGroupSettings: {
                AdMarkers: [],
                CaptionLanguageSetting: "OMIT",
                CaptionLanguageMappings: [],
                HlsCdnSettings: {
                  HlsWebdavSettings: {
                    NumRetries: 10,
                    ConnectionRetryInterval: 1,
                    RestartDelay: 15,
                    FilecacheDuration: 300,
                    HttpTransferMode: "NON_CHUNKED"
                  }
                },
                InputLossAction: "EMIT_OUTPUT",
                ManifestCompression: "NONE",
                Destination: {
                  DestinationRefId: "ti54mj"
                },
                IvInManifest: "INCLUDE",
                IvSource: "FOLLOWS_SEGMENT_NUMBER",
                ClientCache: "ENABLED",
                TsFileMode: "SEGMENTED_FILES",
                ManifestDurationFormat: "FLOATING_POINT",
                SegmentationMode: "USE_SEGMENT_DURATION",
                RedundantManifest: "DISABLED",
                OutputSelection: "MANIFESTS_AND_SEGMENTS",
                StreamInfResolution: "INCLUDE",
                IFrameOnlyPlaylists: "DISABLED",
                IndexNSegments: 10,
                ProgramDateTime: "EXCLUDE",
                ProgramDateTimePeriod: 600,
                KeepSegments: 21,
                SegmentLength: 10,
                TimedMetadataId3Frame: "PRIV",
                TimedMetadataId3Period: 10,
                HlsId3SegmentTagging: "DISABLED",
                CodecSpecification: "RFC_4281",
                DirectoryStructure: "SINGLE_DIRECTORY",
                SegmentsPerSubdirectory: 10000,
                Mode: "LIVE"
              }
            },
            Name: "mk_hls",
            Outputs: [
              {
                OutputSettings: {
                  HlsOutputSettings: {
                    NameModifier: "_1",
                    HlsSettings: {
                      StandardHlsSettings: {
                        M3u8Settings: {
                          AudioFramesPerPes: 4,
                          AudioPids: "492-498",
                          NielsenId3Behavior: "NO_PASSTHROUGH",
                          PcrControl: "PCR_EVERY_PES_PACKET",
                          PmtPid: "480",
                          ProgramNum: 1,
                          Scte35Pid: "500",
                          Scte35Behavior: "NO_PASSTHROUGH",
                          TimedMetadataPid: "502",
                          TimedMetadataBehavior: "NO_PASSTHROUGH",
                          VideoPid: "481"
                        },
                        AudioRenditionSets: "program_audio"
                      }
                    },
                    H265PackagingType: "HVC1"
                  }
                },
                OutputName: "ncyujg",
                VideoDescriptionName: "video_rq0wix",
                AudioDescriptionNames: [
                  "audio_yxa4e6"
                ],
                CaptionDescriptionNames: []
              }
            ]
          }
        ],
        TimecodeConfig: {
          Source: "EMBEDDED"
        },
        VideoDescriptions: [
          {
            CodecSettings: {
              H264Settings: {
                AfdSignaling: "NONE",
                ColorMetadata: "INSERT",
                AdaptiveQuantization: "MEDIUM",
                Bitrate: 3200000,
                EntropyEncoding: "CABAC",
                FlickerAq: "ENABLED",
                FramerateControl: "INITIALIZE_FROM_SOURCE",
                GopBReference: "DISABLED",
                GopClosedCadence: 1,
                GopNumBFrames: 2,
                GopSize: 90,
                GopSizeUnits: "FRAMES",
                SubgopLength: "FIXED",
                ScanType: "PROGRESSIVE",
                Level: "H264_LEVEL_AUTO",
                LookAheadRateControl: "MEDIUM",
                MaxBitrate: 3200000,
                NumRefFrames: 1,
                ParControl: "INITIALIZE_FROM_SOURCE",
                Profile: "MAIN",
                RateControlMode: "QVBR",
                QvbrQualityLevel: 8,
                Syntax: "DEFAULT",
                SceneChangeDetect: "ENABLED",
                SpatialAq: "ENABLED",
                TemporalAq: "ENABLED",
                TimecodeInsertion: "DISABLED"
              }
            },
            Height: 720,
            Name: "video_rq0wix",
            RespondToAfd: "NONE",
            Sharpness: 50,
            ScalingBehavior: "DEFAULT",
            Width: 1080
          }
        ]
      },

      InputSpecification: {
        Codec: "AVC",
        Resolution: "HD",
        MaximumBitrate: "MAX_20_MBPS"
      },
      LogLevel: 'INFO',
      Name: reqParams.channelName,
      RequestId: reqParams.requestId,
      RoleArn: 'arn:aws:iam::705991774343:role/MediaLiveAccessRole',
      Tags: {
      },
      ChannelClass: "STANDARD",

    }
  }


  //console.log('createChannelmedialive is params in utility ' + JSON.stringify(params));
  medialive.createChannel(params, function (err, data) {
    //console.log("IN CREATE CHANNELMEDIALIVE API REF")
    if (err) {
      reject(err)
      //console.log("IN ERROR"+ err.statusCode)
    } else {
      //console.log('FINAL Channel medialive  ' + JSON.stringify(data));
      resolve(data)
    }
  });

})

//aws media live channel start channel method 
exports.startChannel = reqParams => new Promise((resolve, reject) => {

  var params = {
    ChannelId: reqParams.channel_ID
  };

  // console.log("startChannel" + (params))
  medialive.startChannel(params, function (err, data) {
    if (err) {
      console.log(err, err.stack); // an error occurred
      reject()
    }
    else {
      console.log(data);           // successful response
      resolve(data)
    }
  });



})

//aws media live stop channel method 
exports.stopChannel = reqParams => new Promise((resolve, reject) => {

  var params = {
    ChannelId: reqParams.channel_ID
  };

  // console.log("stopChannel" + (params))
  medialive.stopChannel(params, function (err, data) {
    if (err) {
      console.log(err, err.stack); // an error occurred
      reject()
    }
    else {
      console.log(data);           // successful response
      resolve(data)
    }
  });

})

//aws media package describe channel 
exports.describeChannel = reqParams => new Promise((resolve, reject) => {

  var params = {
    Id: reqParams.Id,
    // Id: reqParams.Id,
  };

  //console.log("describeChannel" + JSON.stringify(params))

  mediapackage.describeChannel(reqParams, function (err, data) {
    if (err) {
      reject(err)
    } else {
      // console.log('List Of channels is ' + JSON.stringify(data));
      resolve(data)
    }
  });


})


//aws media package endpoint listing 
exports.listOriginEndpoints = reqParams => new Promise((resolve, reject) => {

  var params = {
    ChannelId: reqParams.ChannelId
  };

  //console.log("listOriginEndpoints UTILITY response" + JSON.stringify(params))

  mediapackage.listOriginEndpoints(reqParams, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //  console.log('List Of channels is ' + JSON.stringify(data));
      resolve(data)
    }
  });


})

//aws system manager for media live password 
exports.putParameterPassword_1 = reqParams => new Promise((resolve, reject) => {

  var params = {
    Name: reqParams.Name,
    Type: 'SecureString',
    Value: reqParams.Value,
    Overwrite: true,
    Tags: [
      {
        Key: reqParams.Key,
        Value: reqParams.Value,
      },
      /* more items */
    ],

  };
  //console.log("putParameter UTILITY response" + JSON.stringify(params))

  ssm.putParameter(reqParams, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //console.log('putParameter is ' + JSON.stringify(data));
      resolve(data)
    }
  });


})

//aws system manager for media live password 
exports.putParameterPassword_2 = reqParams => new Promise((resolve, reject) => {

  var params = {
    Name: reqParams.Name,
    Type: 'SecureString',
    Value: reqParams.Value,
    Overwrite: true,
    Tags: [
      {
        Key: reqParams.Key,
        Value: reqParams.Value,
      },
      /* more items */
    ],

  };
  //console.log("putParameter UTILITY response" + JSON.stringify(params))

  ssm.putParameter(reqParams, function (err, data) {
    if (err) {
      reject(err)
    } else {
      //console.log('putParameter is ' + JSON.stringify(data));
      resolve(data)
    }
  });


})