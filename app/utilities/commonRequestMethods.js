const axios = require('axios');


exports.getApiRequest = reqParams => new Promise((resolve, reject) => {

    axios.get(reqParams.url)
        .then((response) => {
            console.log("success in getApiRequest")
            resolve(response.data) // handle success
        })
        .catch((error) => {
            console.log("ERROR in getApiRequest > " + error)
            reject() // handle error
        })
        .finally(() => {
            // always executed
        });
})

exports.getApiRequestWithHeaders = reqParams => new Promise((resolve, reject) => {

    axios.get(reqParams.url, { params: {}, headers: reqParams.headers })
        .then(function (response) {
            console.log("success in getApiRequest")
            resolve(response.data) // handle success
        })
        .catch(function (error) {
            console.log("ERROR in getApiRequestWithHeaders > " + JSON.stringify(error))
            reject() //handle error
        })
        .finally(function () {
            //always executed
        });
})


exports.postApiRequest = reqParams => new Promise((resolve, reject) => {

    axios.post(reqParams.url,
        reqParams.data
    )

        .then(function (response) {
            console.log("success in postApiRequest")
            resolve(response.data) // handle success
        })
        .catch(function (error) {
            console.log("ERROR in postApiRequest > " + error)
            reject() // handle error
        })
        .finally(function () {
            // always executed
        });
})

exports.postApiRequestWithHeader = reqParams => new Promise((resolve, reject) => {
    axios.post(reqParams.url, reqParams.data, {headers: reqParams.headers })

        .then(function (response) {
            resolve(response.data) // handle success
        })
        .catch(function (error) {
            console.log("ERROR in postApiRequestWithHeader > " + JSON.stringify(error))
            reject() // handle error
        })
        .finally(function () {
            // always executed
        });
})


exports.deleteApiRequest = reqParams => new Promise((resolve, reject) => {
    axios({
        method: 'delete',
        url: reqParams.url,
        headers: { //We can define headers too
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then(function (response) {
            resolve(response) // handle success
        })
        .catch(function (error) {
            console.log("ERROR in deleteApiRequest > " + JSON.stringify(error))
            reject() // handle error
        })
        .finally(function () {
            // always executed
        });
})