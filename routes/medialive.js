var express = require('express');
var router = express.Router();

const ntp = require('ntp2');

const AWS = require("aws-sdk");

const ses = new AWS.SES();
var cron = require('node-cron');



router.get('/', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('medialive', { userData: userData, userName: userName, userRole: userRole });
});


// Media Live Channels listing [ Get ] 
router.get('/listChannels', function (req, res, next) {

  //  console.log("\n \n \n in router of inputlisting")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  // let params = {}
  let params


  inputlist.listChannels(params).then((response) => {

    //RESPONSE
    if (response) {

      //console.log("listInputs" + JSON.stringify(response))

      res.json({
        status: 200,
        message: "listChannels fetched successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "listChannels fetched failed",
        data: null
      })
    }
  });
});




// Media Package Channels Listing [ GET ]
router.get('/mediapackagelistChannels', function (req, res, next) {

  // console.log("\n \n \n in router of mediapackagelistChannels")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  let params = {}

  inputlist.listChannelsmediapackage(params).then((response) => {

    //RESPONSE
    if (response) {

      // console.log("listChannelsmediapackage" + JSON.stringify(response))

      res.json({
        status: 200,
        message: "listChannelsmediapackages fetched successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "listChannelsmediapackage fetched failed",
        data: null
      })
    }
  });
});



//  Media Live Security Group Listing [ GET :- Input Creation ]
router.get('/SecurityGroupList', function (req, res, next) {

  // console.log("\n \n \n in router of securitygrouplisting")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  var params = {
    MaxResults: '7',

  };


  inputlist.listInputSecurityGroups(params).then((response) => {

    //RESPONSE
    if (response) {

      //  console.log("listInputSecurityGroups In route success " + JSON.stringify(response))

      res.json({
        status: 200,
        message: "listInputSecurityGroups fetched successfully",
        data: data

      })
    } else {
      res.json({
        status: 400,
        message: "listInputSecurityGroups fetched failed",
        data: null
      })
    }
  });
});


// Media Package Channel Creation [ POST ]
router.post('/createChannel', function (req, res, next) {

  // console.log("\n \n \n in router of createChannel")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  var params = {
    Id: req.body.Inputname,
    description: req.body.description ? req.body.description : '-',
  };


  inputlist.createChannel(params).then((response) => {

    //RESPONSE
    if (response) {

      //  console.log("createChannel mediapackage response " + JSON.stringify(response))

      res.json({
        status: 200,
        message: "createChannel fetched successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "createChannel fetched failed",
        data: null
      })
    }
  });
});


// Media Live Channel Creation [ POST ]  
router.post('/createChannelmedialive', function (req, res, next) {

  //console.log("\n \n \n in router of create channel")

  //console.log("\n \n \n in router of create channel >> " + JSON.stringify(req.body))

  let inputlist = require('../app/utilities/medialive/medialive.js');

  let uiparams = req.body;

  var params = {
    channelName: uiparams.channelName,
    Input_ID: uiparams.Input_ID,
    Input_Name: uiparams.Input_Name,
    URL_A: uiparams.URL_A,
    username_A: uiparams.username_A,
    password_A: '/medialive/' + uiparams.Input_Name + '_password1',
    URL_B: uiparams.URL_B,
    username_B: uiparams.username_B,
    password_B: '/medialive/' + uiparams.Input_Name + '_password2',
    requestId: uiparams.requestId

  }

  //console.log("\n \n \n in router of create channel params>> " + JSON.stringify(params))


  inputlist.createChannelmedialive(params).then((response) => {
    //console.log("createChannelmedialive " + JSON.stringify(response))
    if (response) {
      console.log('channels fetched successfully');
      res.json({
        status: 200,
        message: 'channels fetched successfully',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error: ${err}`);
    res.json({
      status: 401,
      message: 'channels fetched failed',
      data: null,
    });
  });
});



// Media Live Start Channel [ POST ]
router.post('/startChannel', function (req, res, next) {

  // console.log("\n \n \n in router of startChannel")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  var params = {
    channel_ID: req.body.channel_ID,

  };


  inputlist.startChannel(params).then((response) => {
    //  console.log("startChannel "+ JSON.stringify(response ))
    if (response) {
      console.log('startChannel successfully');
      res.json({
        status: 200,
        message: 'startChannel successfully',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error: ${err}`);
    res.json({
      status: 401,
      message: 'startChannel failed',
      data: null,
    });
  });

});



// Media Live Stop Channel [ POST ]
router.post('/stop-Channel', function (req, res, next) {

  // console.log("\n \n \n in router of stopChannel")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  var params = {
    channel_ID: req.body.channel_ID,
  };

  inputlist.stopChannel(params).then((response) => {
    //console.log("stopChannel "+ JSON.stringify(response ))
    if (response) {
      console.log('stopChannel successfully');
      res.json({
        status: 200,
        message: 'stopChannel successfully',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error: ${err}`);
    res.json({
      status: 401,
      message: 'stopChannel failed',
      data: null,
    });
  });
});



// Media Package CREATE ENDPOINT [ POST ]
router.post('/createChannelEndpoint', function (req, res, next) {

  // console.log("\n \n \n in router of createChannelEndpoint")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  var params = {
    id: req.body.id, /* required */
    packageID: req.body.packageID, /* required */

  };

  //console.log("\n \n \n in router params of createChannelEndpoint"+JSON.stringify(params))


  inputlist.createOriginEndpoint(params).then((response) => {

    //RESPONSE
    if (response) {

      // console.log("Instance listing success " + JSON.stringify(response))

      res.json({
        status: 200,
        message: "createChannel fetched successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "createChannel fetched failed",
        data: null
      })
    }
  });
});


// AWS System Maneger  putParameter For Media Live Channel Creation Password_1[ POST ]
router.post('/putParameterPassword_1', function (req, res, next) {

  //console.log("\n \n \n in router of putParameter")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  var params = {
    Name: '/medialive/' + req.body.channelId + '_password1',
    Type: 'SecureString',
    Value: req.body.password_A,
    Tags: [
      {
        Key: req.body.channelId,
        Value: req.body.password_A,
      },
      /* more items */
    ],

  };

  //console.log("\n \n \n in router params of createChannelEndpoint"+JSON.stringify(params))


  inputlist.putParameterPassword_1(params).then((response) => {

    //RESPONSE
    if (response) {

      // console.log("Instance listing success " + JSON.stringify(response))
      res.json({
        status: 200,
        message: "putParameter successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "putParameter failed",
        data: null
      })
    }
  });
});


//AWS System Maneger  putParameter For Media Live Channel Creation Password_2[ POST ]
router.post('/putParameterPassword_2', function (req, res, next) {

  //console.log("\n \n \n in router of putParameter")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  var params = {
    Name: '/medialive/' + req.body.channelId + '_password2',
    Type: 'SecureString',
    Value: req.body.password_B,
    Tags: [
      {
        Key: req.body.channelId,
        Value: req.body.password_B,
      },
      /* more items */
    ],

  };

  //console.log("\n \n \n in router params of createChannelEndpoint"+JSON.stringify(params))


  inputlist.putParameterPassword_2(params).then((response) => {

    //RESPONSE
    if (response) {

      // console.log("Instance listing success " + JSON.stringify(response))

      res.json({
        status: 200,
        message: "putParameter successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "putParameter failed",
        data: null
      })
    }
  });
});


module.exports = router;
