var express = require('express');
var router = express.Router();

const ntp = require('ntp2');

const AWS = require("aws-sdk");

const ses = new AWS.SES();
var cron = require('node-cron');

router.get('/', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('mediaPackageDescribe',  {userData: userData, userName: userName, userRole: userRole });
});





/* GET mediaPackage Describe listing. */
router.post('/mediaPackageDescribe', function (req, res, next) {

    //console.log("\n \n \n in router of mediaPackageDescribe")
  
    let inputlist = require('../app/utilities/medialive/medialive.js');
    
    
   
    var params = {
      Id: req.body.Id,
      
    };
  
  
  inputlist.describeChannel(params).then((response) => {
  
      //RESPONSE
      if (response) {
  
       //console.log("mediaPackageDescribe In route success " + JSON.stringify(response))
  
        res.json({
          status: 200,
          message: "mediaPackageDescribe fetched successfully",
          data: response
          
        })
      } else {
        res.json({
          status: 400,
          message: "mediaPackageDescribe fetched failed",
          data: null
        })
      }
    });
  });
  


  /* GET mediaPackage Describe listing. */
router.post('/mediaPackagechannelEndpoints', function (req, res, next) {

  //console.log("\n \n \n in router of listOriginEndpoints")

  let inputlist = require('../app/utilities/medialive/medialive.js');
  
  
 
  var params = {
    ChannelId: req.body.ChannelId,
    
  };


inputlist.listOriginEndpoints(params).then((response) => {

    //RESPONSE
    if (response) {

     //console.log("listOriginEndpoints In route success " + JSON.stringify(response))

      res.json({
        status: 200,
        message: "listOriginEndpoints fetched successfully",
        data: response
        
      })
    } else {
      res.json({
        status: 400,
        message: "listOriginEndpoints fetched failed",
        data: null
      })
    }
  });
});




 


module.exports = router;
