var express = require('express');
var router = express.Router();

const ntp = require('ntp2');

const AWS = require("aws-sdk");

const ses = new AWS.SES();
var cron = require('node-cron');


router.get('/', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('createInput', { userData: userData, userName: userName, userRole: userRole });
});



// Media Live Input Creation 
router.post('/CreateInput', function (req, res, next) {

  // console.log("\n \n \n in router of CreateInput")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  // let params = {}

  var params = {
    Name: req.body.Inputname,
    //  InputSecurityGroups  :req.body.Input_security_group,
    Type: req.body.Input_type,
    application_A: req.body.application_A,
    instance_A: req.body.instance_A,
    application_B: req.body.application_B,
    instance_B: req.body.instance_B,

    //For VPC 
    // Vpc: {

    //   SecurityGroupIds: [
    //     req.body.Input_security_group,
    //     /* more items */
    //   ]
    // }
  };

  //console.log("createInput success params " + JSON.stringify(params))


  inputlist.createInput(params).then((response) => {

    //RESPONSE
    if (response) {

      //  console.log("createInput success " + JSON.stringify(response))

      res.json({
        status: 200,
        message: "createInput successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "createInput failed",
        data: null
      })
    }
  });
});


// Media Live Input Listing [ GET ]
router.get('/inputlisting', function (req, res, next) {

  //console.log("\n \n \n in router of inputlisting")

  let inputlist = require('../app/utilities/medialive/medialive.js');

  var params = {
   // MaxResults: '5',

  };



  inputlist.listInputs(params).then((response) => {

    //RESPONSE
    if (response) {

      //  console.log("listInputs" + JSON.stringify(response))

      res.json({
        status: 200,
        message: "inputlisting fetched successfully",
        data: response
      })
    } else {
      res.json({
        status: 400,
        message: "inputlisting fetched failed",
        data: null
      })
    }
  });
});



module.exports = router;
