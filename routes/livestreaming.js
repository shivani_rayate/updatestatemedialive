var express = require('express');
var router = express.Router();




const fs = require('fs');
const AWS = require("aws-sdk");
const request = require('request');

// var parseXML = require("xml-parse-from-string")
var xml2js = require('xml2js');
var parser = new xml2js.Parser();

var parseString = require('xml2js').parseString;


var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const xhr = new XMLHttpRequest();
var utf8 = require('utf8');

// let requestMethods = require("../app/utilities/commonRequestMethods.js");



router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('livestreaming', { userData: userData, userName: userName, userRole: userRole });

});


// Create Channel On Primary Server Then MongoDB
router.post('/create_Channel_On_Primary', (req, res, next) => {
    console.log("\n  I am in route channel creation ")


    var xmlData = req.body.xmlData;
    var xmlFileName = req.body.channelName;
    var channelName = req.body.channelName;
    var ServerIp = req.body.serverIp;
    var url = `http://${ServerIp}/${channelName}/${xmlFileName}.isml`

    request.post({
        url: `http://${ServerIp}:3001/${channelName}/${xmlFileName}.isml`,
        method: "POST",
        headers: {
            'Content-Type': 'application/xml',
        },
        body: xmlData,
    },
        function (error, response, body) {
    
            if (response.statusCode === 201) {

                let params = {
                    url: url,
                    channelName: req.body.channelName ? req.body.channelName : null,
                    creator: req.body.creator ? req.body.creator : null,
                    lookaheadFragments: req.body.lookaheadFragments ? req.body.lookaheadFragments : null,
                    dvrWindowLength: req.body.dvrWindowLength ? req.body.dvrWindowLength : null,
                    archiveSegmentLength: req.body.archiveSegmentLength ? req.body.archiveSegmentLength : null,
                    archiving: req.body.archiving ? req.body.archiving : null,
                    archiveLength: req.body.archiveLength ? req.body.archiveLength : null,
                    restartOnEncoderReconnect: req.body.restartOnEncoderReconnect ? req.body.restartOnEncoderReconnect : null,
                    issMinimumFragmentLength: req.body.issMinimumFragmentLength ? req.body.issMinimumFragmentLength : null,
                    hlsMinimumFragmentLength: req.body.hlsMinimumFragmentLength ? req.body.hlsMinimumFragmentLength : null,
                    objectId: req.body.objectId ? req.body.objectId : null,
                    ServerIp: req.body.serverIp ? req.body.serverIp : null,
                    serverName: req.body.serverName ? req.body.serverName : null
                }
                channelObj.save(params).then((_response) => {
                    if (_response) {
                        console.log('channel data saved');
                        res.json({
                            status: 200,
                            message: 'channel Saved successfuly',
                            data: _response,
                        })

                    } else {
                        console.log('channel data save failed');
                        res.json({
                            status: 401,
                            message: 'channel Save failed',
                            data: null,
                        })
                    }
                })
            } else {
                console.log("Operation Failed")
            }
            console.log("body >> " + body);
            console.log(error);
        });
})

router.post('/create_Channel_On_Backup', (req, res, next) => {
    console.log("\n  I am in route channel creation ")
    // console.log("\n Data Receive route through UI:- " + req.body.xmlData)

    // var xmlData = '<?xml version="1.0" encoding="utf-8"?>  <smil xmlns="http://www.w3.org/2001/SMIL20/Language">   <head>   <meta name="clientManifestRelativePath" content="sony123.ismc" />     <meta name="creator" content="Unified Streaming Platform (USP)" />     <meta name="lookahead_fragments" content="2" />     <meta name="dvr_window_length" content="30" />     <meta name="archive_segment_length" content="0" />     <meta name="archiving" content="false" />   </head>   <body>     <switch>     </switch>   </body> </smil> ';

    // <?xml version="1.0" encoding="utf-8"?> <smil xmlns="http://www.w3.org/2001/SMIL20/Language">   <head>   <meta name="clientManifestRelativePath" content="${params.channelName}.ismc" />        <meta name="creator" content="${params.creator}" />        <meta name="lookahead_fragments" content="${params.lookaheadFragments}" />       <meta name="dvr_window_length" content="${params.dvrWindowLength}" />      <meta name="archive_segment_length" content="${params.archiveSegmentLength}" />      <meta name="archiving" content="${params.archiving}" />     </head>   <body>    <switch>     </switch>    </body> </smil>`;
    var xmlData = req.body.xmlData;
    var xmlFileName = req.body.xmlFileName;
    var channelName = req.body.channelName;
    var url = BASE_Primary + `/${channelName}/${xmlFileName}.isml`
    var backupserverIp = req.body.backupserverIp;

    request.post({
        url: `http://${backupserverIp}:3001/${channelName}/${xmlFileName}.isml`,
        method: "POST",
        headers: {
            'Content-Type': 'application/xml',
        },
        body: xmlData,
    },
        function (error, response, body) {
            if (response.statusCode === 201) {

                let params = {
                    url: url,
                    channelName: req.body.channelName ? req.body.channelName : null,
                    creator: req.body.creator ? req.body.creator : null,
                    lookaheadFragments: req.body.lookaheadFragments ? req.body.lookaheadFragments : null,
                    dvrWindowLength: req.body.dvrWindowLength ? req.body.dvrWindowLength : null,
                    archiveSegmentLength: req.body.archiveSegmentLength ? req.body.archiveSegmentLength : null,
                    archiving: req.body.archiving ? req.body.archiving : null,
                    archiveLength: req.body.archiveLength ? req.body.archiveLength : null,
                    restartOnEncoderReconnect: req.body.restartOnEncoderReconnect ? req.body.restartOnEncoderReconnect : null,
                    issMinimumFragmentLength: req.body.issMinimumFragmentLength ? req.body.issMinimumFragmentLength : null,
                    hlsMinimumFragmentLength: req.body.hlsMinimumFragmentLength ? req.body.hlsMinimumFragmentLength : null,
                    objectId: req.body.objectId ? req.body.objectId : null,
                    backupserverIp: req.body.backupserverIp ? req.body.backupserverIp : null,
                    backupserverName : req.body.backupserverName ? req.body.backupserverName : null

                }
                backupchannelObj.save(params).then((_response) => {
                    if (_response) {
                        console.log('channel data saved');
                        res.json({
                            status: 200,
                            message: 'channel Saved successfuly',
                            data: _response,
                        })

                    } else {
                        console.log('channel data save failed');
                        res.json({
                            status: 401,
                            message: 'channel Save failed',
                            data: null,
                        })
                    }
                })
            } else {
                console.log("Operation Failed")
            }
            console.log("body >> " + body);
            console.log(error);
        });
})


// Channel Delete On Server Then MongoDB
router.post('/channel-delete', (req, res, next) => {
    console.log("\n \n \n \n  channel-delete")
    const params = {
        channelName: req.body.channelName,
    }
    var channelName = req.body.channelName;
    var serverIp = req.body.serverip;
    
    request.delete({
        url: `http://${serverIp}:3001/${channelName}/${channelName}.isml`,
        method: "DELETE",
    },
        function (error, response, body) {
            console.log("delete"+response.statusCode);
            if (response.statusCode === 200) {

                channelObj.deleteChannel(params).then((data) => {
                    res.json({
                        status: 200,
                        message: 'Delete-channel successfully',
                        data: data,
                    });
                });

            } else {
                console.log("Operation Failed")
            }
            console.log(body);
            console.log(error);
        });
})


router.post('/update-channel', (req, res, next) => {

    const params = {
        channelName: req.body.channelName
    };
    channelObj.findDataByid(params).then((data) => {
        res.json({
            status: 200,
            message: 'Metadata Loaded successfully',
            data: data,
        });
    });

})


router.post('/channel-listing-byIp', (req, res, next) => {

    const params = {
        ServerIp: req.body.ServerIp
    };
    channelObj.findDataByIp(params).then((data) => {
        res.json({
            status: 200,
            message: 'channel listing by ip fetched successfully',
            data: data,
        });
    });
})


// Update Channel On Server Then MongoDB
router.post('/update-channel-params', (req, res, next) => {

    var xmlUpdatedData = req.body.xmlUpdatedData;
    var xmlFileName = req.body.xmlFileName;
    var UpdatedchannelName = req.body.UpdatedchannelName;
    var UpdatedobjectId = req.body.UpdatedobjectId;
    var UpdatedServerIp = req.body.updatedServerIp

    console.log("\n  Final Parsing Data:- " + xmlFileName)
    console.log("\n  Final Parsing Data channelName:- " + UpdatedchannelName)
    console.log("\n  objectId:- " + UpdatedobjectId)
    console.log("\n  UpdatedServerIp:- " + UpdatedServerIp)

    request.put({
        url: `http://${UpdatedServerIp}:3001/${UpdatedchannelName}/${xmlFileName}.isml`,
        method: "PUT",
        headers: {
            'Content-Type': 'application/xml',
        },
        body: xmlUpdatedData,
    },
        function (error, response, body) {

            if (response.statusCode === 200) {

                const params = {

                    xmlFileName: req.body.xmlFileName ? req.body.xmlFileName : null,
                    creator: req.body.Updatedcreator ? req.body.Updatedcreator : null,
                    lookaheadFragments: req.body.UpdatedlookaheadFragments ? req.body.UpdatedlookaheadFragments : null,
                    dvrWindowLength: req.body.UpdateddvrWindowLength ? req.body.UpdateddvrWindowLength : null,
                    archiveSegmentLength: req.body.UpdatedarchiveSegmentLength ? req.body.UpdatedarchiveSegmentLength : null,
                    archiving: req.body.Updatedarchiving ? req.body.Updatedarchiving : null,
                    archiveLength: req.body.UpdatedarchiveLength ? req.body.UpdatedarchiveLength : null,
                }

                // console.log("mongo db channel params" +JSON.stringify(params))

                channelObj.findOneAndUpdate(params).then((_response) => {
                    if (_response) {
                        console.log('Updated channel data saved');
                        res.json({
                            status: 200,
                            message: 'Updated channel Saved successfuly',
                            data: _response,
                        })
                    }
                }).catch((err) => {
                    console.log(`error: ${err}`);
                    res.json({
                        status: 401,
                        message: 'Updated channel Save failed',
                        data: null,
                    });
                });
            }
            console.log(response.statusCode);
            console.log(body);
            console.log(error);
        });

})


router.get('/channel_listing', (req, res, next) => {

    channelObj.findall().then((data) => {
        console.log("channel_listing "+ JSON.stringify(data ))
        if (data) {
            console.log('channels fetched successfully');
            res.json({
                status: 200,
                message: 'channels fetched successfully',
                data: data,
            })
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'channels fetched failed',
            data: null,
        });
    });

})


// Get Channel State Listing
router.post('/get_channel_state', (req, res, next) => {

    var channelName = req.body.channel_Name
    var serverIp = req.body.serverip

    request.get({
        url: `http://${serverIp}/${channelName}/${channelName}.isml/state`,
        method: "GET",
    },
        function (error, response, body) {

            if (!error && response.statusCode == 200) {
                var xml = response.body
                parseString(xml, function (err, result) {
                    res.json({
                        status: 200,
                        message: 'channel get state successfuly',
                        data: result,

                       

                    })
                      
                    // const state_params = {

                    //     channelName: channelName,
                    //     response: result
                    // }
    
                  //  console.log("Response 2"+ JSON.stringify(state_params))

                

                })
            } else {
                console.log('channel get failed');
                res.json({
                    status: 401,
                    message: 'channel get state  failed',
                    data: null,
                })
            }
        }
    )
});


router.post('/channel_statistics', (req, res, next) => {

    console.log("I m in route channel_statistics")
    var channelName = req.body.channelName;
    var serverIp = req.body.serverip;
 //   console.log("serverIp " + JSON.stringify(serverIp))

    request.get({
        url: `http://${serverIp}/${channelName}/${channelName}.isml/statistics`,
        method: "GET",
    },
        function (error, response, body) {
            if (response.statusCode === 200) {
                var xml = response.body
                parseString(xml, function (err, result) {
                    res.json({
                        status: 200,
                        message: 'channel get successfuly',
                        data: result,
                    })
                });
            } else {
                console.log('channel get failed' + error);
                res.json({
                    status: 401,
                    message: 'channel update Save failed',
                    data: null,
                })
            }
        }
    )
});


router.get('/backupchannel_listing', (req, res, next) => {

    backupchannelObj.findall().then((data) => {
        console.log("backupchannel_listing "+ JSON.stringify(data ))
        if (data) {
            console.log('backupchannel_listing fetched successfully');
            res.json({
                status: 200,
                message: 'backupchannel_listing fetched successfully',
                data: data,
            })
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'channels fetched failed',
            data: null,
        });
    });

})


router.get('/totalchannel_listing', (req, res, next) => {

    backupchannelObj.findall().then((data) => {
        console.log("backupchannel_listing "+ JSON.stringify(data ))
        if (data) {
            console.log('backupchannel_listing fetched successfully');
            res.json({
                status: 200,
                message: 'backupchannel_listing fetched successfully',
                data: data,
            })
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'channels fetched failed',
            data: null,
        });
    });

    channelObj.findall().then((data) => {
        console.log("channel_listing "+ JSON.stringify(data ))
        if (data) {
            console.log('channels fetched successfully');
            res.json({
                status: 200,
                message: 'channels fetched successfully',
                data: data,
            })
        }
    }).catch((err) => {
        console.log(`error: ${err}`);
        res.json({
            status: 401,
            message: 'channels fetched failed',
            data: null,
        });
    });

 

})



// router.post('/channelstate_save', (req, res, next) => {


//     const state_params = {

//         state : req.body.state,
//         channelName: req.body.channel_Name,
       
//     }

//     console.log("state_params 2"+ JSON.stringify(state_params))

//     channelStateObj.save(state_params).then((_response) => {
//         if (_response) {
//             console.log('channels fetched successfully');
//             res.json({
//                 status: 200,
//                 message: 'channels fetched successfully',
//                 data: _response,
//             })
//         }
//     }).catch((err) => {
//         console.log(`error: ${err}`);
//         res.json({
//             status: 401,
//             message: 'channels fetched failed',
//             data: null,
//         });
//     });

// })



// router.get('/getchannelstatelisting', (req, res, next) => {

//     channelStateObj.findall().then((data) => {
//         if (data) {
//             console.log('channels fetched successfully');
//             res.json({
//                 status: 200,
//                 message: 'channels fetched successfully',
//                 data: data,
//             })
//         }
//     }).catch((err) => {
//         console.log(`error: ${err}`);
//         res.json({
//             status: 401,
//             message: 'channels fetched failed',
//             data: null,
//         });
//     });

// })

module.exports = router;
