

// media package Channel Creation
function create_Channel_On_mediapackage(_params) {
    $.post('/v1/medialive/createChannel', _params,

        function (data, textStatus, request) {
            if (data.status === 200) {

                _data = data.data
                // console.log("Channel_On_mediapackage _data" + JSON.stringify(_data))

                let params = {
                    id: _params.Inputname,
                    packageID: data.data.Id
                }

                //console.log("Channel_On_mediapackage" + JSON.stringify(params))
                // alert("Channel_On_mediapackage")

                //create endpoint 
                $.post(`/v1/medialive/createChannelEndpoint`,
                    params,
                    function (data, textStatus, request) {
                        if (data.status === 200) {

                            _data = data.data

                            // alert("create Channel Endpoint")
                            window.location.href = '/v1/mediaPackagecreateChannel';

                        } else {
                            toastr.danger('Creation failed.')
                        }
                    })

                // Createchannelendpoint(params)

            } else {

            }
        })
}


//media live input form : selection type RTP/RTMP
$('#Inputtype').on('change', function () {
    if (this.value == 'RTMP_PUSH') {
        $('#Inputdestinations_div').removeClass("force-hidden")
    }
    else {
        $("#Inputdestinations_div").addClass("force-hidden");
    }
});


// Input Creation Click Event 
$("#input-creation-form").submit(function (event) {

    let params = {
        Inputname: $('#input-name').val().trim(),
        Input_type: $('#Inputtype').val().trim(),
        //  Input_security_group : $('#securityGroupId').val().trim(),
        application_A: $('#applicationA').val().trim(),
        instance_A: $('#instanceA').val().trim(),
        application_B: $('#applicationB').val().trim(),
        instance_B: $('#instanceB').val().trim()
        // Input_source_A: $('#Input-source-A').val().trim(),
        // Input_source_B : $('#Input-source-B').val().trim(),
    }

    //  alert("Input Parameter params"+ JSON.stringify(params))

    let _params = {
        Inputname: params.Inputname,
        Input_type: params.Input_type,
        //  Input_security_group : params.Input_security_group,
        application_A: params.application_A,
        instance_A: params.instance_A,
        application_B: params.application_B,
        instance_B: params.instance_B
    }

    // console.log("Input Parameter" + JSON.stringify(_params))
    //  alert("Input Parameter"+ JSON.stringify(_params))

    Input_Channel(_params),
        create_Channel_On_mediapackage(_params)

    event.preventDefault()
})

//function called here 
// document.ready
$(function () {

    InputListing()
    ChannelsListingmediapackage()
    listInputSecurityGroups()
    ChannelsListing()
    
    //
    setTimeout(function () {
        ChannelsListingState()
    }, 2000);


})


// media live CreateInput ajax call
function Input_Channel(_params) {
    $.post('/v1/createInput/CreateInput', _params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                _data = data.data

                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Input has been Created!',
                    showConfirmButton: false,
                    timer: 3000
                }).then(function (){
                    window.location.href = '/v1/mediaPackagecreateChannel';
                });
              

            } else {
                toastr.danger('Creation failed.')
            }
        })
}

//media live input listing ajax call
function InputListing() {

    $.get('/v1/createInput/inputlisting',
        function (data, status) {
            if (data.status === 200) {
                let _data = data.data;

                //  console.log("inputlisting"+JSON.stringify(_data))

                var inputlisting_string = '';
                SecurityGroup_stringCount = '';
                for (var i = 0; i < _data.Inputs.length; i++) {

                    inputID = _data.Inputs[i].Name,
                        nameInput = _data.Inputs[i].Id,
                        state = _data.Inputs[i].State
                    SecurityGroup_stringCount++

                    // console.log("inputID ID" + inputID)
                    // console.log("state" + state)

                    if (state === 'DETACHED') {

                        inputlisting_string += '<option class="inputlist" id="Input"  nameInput=' + nameInput + '  >' + inputID + '  </option>'

                    }

                }
                $('#InputlistId').append(inputlisting_string);

                appendInputDatatable(_data)
            } else {

            }
        })


}




//media live channel creation form : select drop down value for input type RTP/RTMP
var packageDetails_;
$("select.selectinput").change(function () {
    var selectedInputType = $(this).children("option:selected").val();

    let params = {

        Id: selectedInputType
    }

    //  alert("Input : -" + JSON.stringify(params))

    //media package Describe Method Oparation
    $.post('/v1/mediaPackageDescribe/mediaPackageDescribe', params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                _data = data.data
                //  console.log("mediaPackageDescribe" + JSON.stringify(_data))
                packageDetails_ = _data;
                appendMediapackageDescribe(_data)

            } else {
                toastr.danger('Creation failed.')
            }
        })
    // alert("You have selected input type - " + selectedInputType);
});

function destroyRows() {
    $('#channellisting_tbody').empty()
    $('#channellisting_table').DataTable().rows().remove();
    $("#channellisting_table").DataTable().destroy()
}



var channel_listing_table;
function reInitializeDataTable() {
    $("#channellisting_table").DataTable().destroy()
    channel_listing_table = $('#channellisting_table').DataTable({
        // "order": [[2, "desc"]], // for descending order
        // "columnDefs": [
        //     { "width": "40%", "targets": 1 }
        // ]
    })
}



//media live get call channel listing include status
function ChannelsListing() {
    return new Promise((resolve, reject) => {
       // alert(" In promis ")
        $.get('/v1/medialive/listChannels',
            function (data, status) {
                if (data.status === 200) {
                    let _data = data.data;
                    console.log("listChannels :>>>>>>" + JSON.stringify(_data))
                 //destroyRows()
                    //alert("ChannelsListing")
                    appendChannellistingDatatable(data)

                    resolve(_data)
                   // alert("Reslove data : " + JSON.stringify(_data))
                } else {

                }
            })
    })
}

// on click of start/stop media live channel this function called 
function ChannelsListingState() {
    
        $.get('/v1/medialive/listChannels',
            function (data, status) {
                if (data.status === 200) {
                   // alert("In channel get listing ")
                    let _data = data.data.Channels;
                    getstate(_data)
                   
                    setTimeout(function () {
                                       
                       // alert("In set timout function calling function ")
                        ChannelsListingState()
                    }, 2000);
                   
                } 
            })
}


//media package channel listing 
function ChannelsListingmediapackage() {

    $.get('/v1/medialive/mediapackagelistChannels',
        function (data, status) {
            if (data.status === 200) {
                let _data = data.data;
                //  console.log("mediapackagelistChannels"+JSON.stringify(_data))
                // destroyRows()
                //appendMediapackageChannellisting(_data)
            } else {

            }
        })
}

//media live input security group listing 
function listInputSecurityGroups() {

    $.get('/v1/createInput/listInputSecurityGroups',
        function (data, status) {
            if (data.status === 200) {
                //  console.log("listInputSecurityGroups"+JSON.stringify(data.data))

                _data = data.data

                appendlistInputSecurityGroups(data)
                var SecurityGroup_string = '';
                SecurityGroup_stringCount = '';
                for (var i = 0; i < _data.InputSecurityGroups.length; i++) {

                    SecurityGroupId = _data.InputSecurityGroups[i].Id;
                    SecurityGroup_stringCount++

                    SecurityGroup_string += '<option class="securityGroupId"  value=' + SecurityGroupId + ' >' + SecurityGroupId + '  Input Security Group  ' + SecurityGroup_stringCount + '</option>'
                    $('#securityGroupId').html(SecurityGroup_string);

                }

            } else {

            }
        });
}



// media live channel creation need to pass UUID [ UUID Generation ] 
function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}


// media live channel creation submit form  
$("#channel-creation-onmedialive-form").submit(function (event) {

    var packageDetails = packageDetails_.HlsIngest.IngestEndpoints
    var requestId = uuidv4()

    var profileType = $('#profile_list').val().trim()

    //  alert("profileType"+ requestId)
    //console.log("uuid" + requestId)


    let params = {

        channelName: $('#ChannelID').val().trim(),
        IAM_role: 'arn:aws:iam::705991774343:role/MediaLiveAccessRole',
        Input_ID: $('#Input').attr('nameInput'),
        Input_Name: $('#InputlistId').val().trim(),


        URL_A: packageDetails[0].Url,
        username_A: packageDetails[0].Username,
        password_A: packageDetails[0].Password,

        URL_B: packageDetails[1].Url,
        username_B: packageDetails[1].Username,
        password_B: packageDetails[1].Password,
        profileType: profileType,
        requestId: requestId

    }


    //POST ajax call For media live channel creation 
    $.post('/v1/medialive/createChannelmedialive',
        params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                _data = data.data
               //alert("Ready !")

               //ChannelsListingState();

                Swal.fire({
                    position: 'centre',
                    type: 'success',
                    title: 'Channel has been Created!',
                    showConfirmButton: false,
                    timer: 3000
                }).then(function (){
                    window.location.href = '/v1/primaryChannelListing'
                   
                });
                

            } else {
                toastr.danger('Creation failed.')
            }
        })

    event.preventDefault()

})


//media live input listing append function 
function appendInputDatatable(data) {

    var array_Input = data.Inputs;

    //console.log("input listing" + JSON.stringify(array_Input))
    var Input_table = "";

    array_Input.forEach(function (element, i) {

        var Arn = element.Arn ? element.Arn : "-";
        var Type = element.Type ? element.Type : "-";
        var Name = element.Name ? element.Name : "-";
        var State = element.State ? element.State : "-";
        var ID = element.Id ? element.Id : "-";
        var Destination_A = element.Destinations[0].Url ? element.Destinations[0].Url : "-";
        var Destination_B = element.Destinations[1].Url ? element.Destinations[1].Url : "-";

        if (State === 'ATTACHED') {

            State = '<i class="mdi mdi-check-circle-outline" style="font-size: 15px !important;color: #50d050 !important;font-style: normal !important;">&nbspAttached</i>'

        } else if (State === 'DETACHED') {
            State = '<i class="mdi mdi-close-circle-outline" style="font-size: 15px !important;color: red !important;font-style: normal !important;">&nbspDetached</i>'
        }

        
        Input_table += `<tr class="channel-overview-tbl-row" data-data="Any Data"> <td class="index">${i + 1}</td>
        <td class="channel-name">${Name}</td>
          <td>${State}</td>
          <td class=""  >${Type}</td>
          <td class="" id="urlA" >${Destination_A}</td>
          <td class="" id="urlA" >${Destination_B}</td>
          </tr>`

        if (i == array_Input.length - 1) {
            $('#input_listing_tbody').append(Input_table)
            // reInitializeDataTable();
        }
    })
}

// media live channel listing append function 
function appendChannellistingDatatable(data) {

    var array = data.data.Channels;

   // console.log("channel listing : >>>>>>>>>>>" + JSON.stringify(array))

    var options_table = "";

    array.forEach(function (element, i) {

        var Arn = element.Arn ? element.Arn : "-";
        var ChannelClass = element.ChannelClass ? element.ChannelClass : "-";
        var Name = element.Name ? element.Name : "-";
        var State = element.State ? element.State : "-";
        console.log("State in get call : >>>>>>> " + JSON.stringify(State))
        var ID = element.Id ? element.Id : "-";
        var FilterStrength = element.InputAttachments[0].InputSettings.FilterStrength ? element.InputAttachments[0].InputSettings.FilterStrength : "-";
        var InputName = element.InputAttachments[0].InputAttachmentName ? element.InputAttachments[0].InputAttachmentName : "-";

        
        options_table += `<tr class="channel-overview-tbl-row" data-data="Any Data">
        
       
        <td class="index">${i + 1}</td>
         <td class="channel-name">${Name}</td>
          <td class="${ID}-state">${State}</td>
          <td class="${Name} text-break"></td>
         
          <td class="${ID}" data-target="#play-video-channel"> </td>
          <td class=""> <button channelId="${ID}" class="startChannel btn btn-icon waves-effect waves-light btn-success btn-sm">Start</button></td>
          <td class=""> <button channelId="${ID}" class="stopChannel btn btn btn-icon waves-effect waves-light btn-danger btn-sm">Stop</button></td>
         
          </tr>`

        if (i == array.length - 1) {

            $('#channellisting_tbody').empty()
            $('#channellisting_tbody').append(options_table)
            
            getstate(array)
           // reInitializeDataTable();

        }

    })

}

//media package ouput url promis-fy function  
var combine_data = []
function getstate(array) {

    //console.log(`\n \n called at ` + new Date() + `array > `)
    //console.log("array data in getstate  :- " + JSON.stringify(array))

    array.forEach(function (element, i) {
        var InputName = element.InputAttachments[0].InputAttachmentName ? element.InputAttachments[0].InputAttachmentName : "-";
        var Name = element.Name ? element.Name : "-";
        var ID = element.Id ? element.Id : "-";
        var State = element.State ? element.State : "-";
        if (State === 'IDLE') {
            $(`.${ID}-state`).html('<span class="badge badge-customblue badge" style=" background-color: #097bd3;font-size: 12px; width: 50px;">Idle</span>')
        }

         else if (State === 'CREATING') {
             $(`.${ID}-state`).html('<span class="badge badge-purple badge" style="font-size: 12px; width: 52px;">Creating</span>')

        } 
        else if (State === 'RUNNING') {
            $(`.${ID}-state`).html('<span class="badge badge-success badge" style="font-size: 12px; width: 52px;">running</span>')
            //console.log("RUNNING : -"+ State)  
        } else if (State === 'STOPPING') {
            $(`.${ID}-state`).html('<span class="badge badge-dark badge" style="font-size: 12px; width: 56px;">Stopping</span>')

        } else if (State === 'STARTING') {
            $(`.${ID}-state`).html('<span class="badge badge-primary" style="font-size: 12px; width: 52px;">Starting</span>')

        }

               
        outputurl(InputName).then((outputUrl) => {
            combine_data.push({channeldata:array,channelurl:outputUrl})

            var channelendpoints = outputUrl.data.OriginEndpoints[0].Url;
           // console.log("combine_data Array Data>>> "+ JSON.stringify(combine_data))
            $(`.${Name}`).html(`${channelendpoints}`);
            $(`.${ID}`).html(`<i ChannelUrl="${channelendpoints}" data-toggle="modal" data-target="#play-video-channel"  class="mdi mdi-play-circle-outline play-video-channelButton" style="color: #097bd3; font-size: 1.5rem;line-height: 0.9;"></i>`)

        })

    })
}

//media package output url POST call
function outputurl(InputName) {
    return new Promise((resolve, reject) => {

        let channel_endpoints_params = {
            ChannelId: InputName
        }
        // ajax POST call 
        $.post('/v1/mediaPackageDescribe/mediaPackagechannelEndpoints', channel_endpoints_params,
            function (data, status) {
                if (data.status === 200) {

                    resolve(data)
                    //  console.log("mediaPackagechannelEndpoints"+JSON.stringify(data))
                    // formatChannelData(data)
                } else {
                    reject()
                    toastr.error(data.message);
                }
            })
    })

}


// media live click event media live start channel 
$(document).on("click", ".startChannel", function () {

    var channel_ID = $(this).attr('channelId');

    // alert("channel_ID" + channel_ID)

    let _params = {

        channel_ID: channel_ID

    }

    // alert("channel_ID" + _params.channel_ID)


    $.post('/v1/medialive/startChannel', _params,

    function (data, status) {
            if (data.status === 200) {
                _data = data.data
//alert("In start channel section : -" )
                ChannelsListingState()

            } else {
                toastr.danger('Creation failed.')
            }
        })


})

// media live click event media live stop channel
$(document).on("click", ".stopChannel", function () {

    var channel_ID = $(this).attr('channelId');
    // alert("channel_ID" + channel_ID)

    let _params = {

        channel_ID: channel_ID

    }

    // alert("channel_ID" + _params.channel_ID)


    $.post('/v1/medialive/stop-Channel', _params,

    function (data, status) {
            if (data.status === 200) {
                
                _data = data.data;
                ChannelsListingState()

            } else {
                toastr.danger('Creation failed.')
            }
        })



})


// media package channel credentials user name , pass ,url 
function appendMediapackageDescribe(_data) {

    var channelId = _data.Id

    var array = _data.HlsIngest.IngestEndpoints;

    //  console.log("appendMediapackageChannellisting" + JSON.stringify(array))

    //HLS destination A credentials
    var id_A = array[0].Id;
    var password_A = array[0].Password;
    var url_A = array[0].Url;
    var username_A = array[0].Username;

    //HLS destination B credentials
    var id_B = array[1].Id;
    var password_B = array[1].Password;
    var url_B = array[1].Url;
    var username_B = array[1].Username;


    // console.log("MediapackageChannellisting id_1 >>>>>>>>>>" + id_A)

    // console.log("MediapackageChannellisting password_1 >>>>>>>>>>" + password_A)
    // console.log("MediapackageChannellisting url_1 >>>>>>>>>>" + url_A)
    // console.log("MediapackageChannellisting username_1 >>>>>>>>>>" + username_A)


    // console.log("MediapackageChannellisting id_2 >>>>>>>>>>" + id_B)

    // console.log("MediapackageChannellisting password_2 >>>>>>>>>>" + password_B)
    // console.log("MediapackageChannellisting url_2 >>>>>>>>>>" + url_B)
    // console.log("MediapackageChannellisting username_2 >>>>>>>>>>" + username_B)


    //meida package params send on submit button
    let params = {

        channelId: channelId,
        password_A: password_A,
        password_B: password_B

    }


    passwordparam1(params)
    passwordparam2(params)

    // createChannelMediaLive(params)

}

//media live password_1 store system manager 
function passwordparam1(params) {

    $.post('/v1/medialive/putParameterPassword_1', params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                _data = data.data

                //   alert("putParameterPassword_1")

            } else {
                toastr.danger('Creation failed.')
            }
        })

}

//media live password_2 store system manager 
function passwordparam2(params) {

    $.post('/v1/medialive/putParameterPassword_2', params,

        function (data, textStatus, request) {
            if (data.status === 200) {
                _data = data.data
                //  alert("putParameterPassword_2")

            } else {
                toastr.danger('Creation failed.')
            }
        })

}


// video js player 
var player;
//  Initiate publishing channel Player
function initiatePlayer(params) {

    return new Promise((resolve, reject) => {

        // console.log("called with > id " + params.player_id)
        // console.log("called with > url " + params.url)
        //  console.log("called with > channelName " + params.channelName)

        if (player !== undefined) {
            player.reset()
        }

        var options = {
            plugins: {

                controlBar: {
                    progressControl: {

                        MouseTimeDisplay: true,
                        PlayProgressBar: true
                    }
                },
                qualityMenu: {
                    useResolutionLabels: true
                }
            }
        };
        player = videojs(params.player_id, options)
        player.src({
            type: 'application/x-mpegURL',
            src: params.url
        });

        player.qualityMenu();
        player.onloadeddata = function () {
            resolve()
        };

    })

}

$(document).on("click", ".play-video-channelButton", function () {

    var publishingplayurl = $(this).attr('ChannelUrl')

    // var publishing_url_video= publishingplayurl + '/.m3u8'
    // var publishing_url_video = 'http://suntv.skandha.tv/KTVHD/KTVHD.isml/.m3u8'

    var params = {
        player_id: 'channelurlplayer',
        url: publishingplayurl,
        // channelName: channelName
    }
    initiatePlayer(params)
})

